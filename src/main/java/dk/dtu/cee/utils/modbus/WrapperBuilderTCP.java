package dk.dtu.cee.utils.modbus;

import dk.dtu.cee.utils.modbus.parameters.Offset;

public class WrapperBuilderTCP extends WrapperBuilderBase {

    private final String address;

    private int port = 502;

    WrapperBuilderTCP(int id, String address) {
        super(id);
        this.address = address;
    }

    public WrapperBuilderTCP offset(Offset offset) {
        this.offset = offset;
        return this;
    }

    public WrapperBuilderTCP port(int port) {
        this.port = port;
        return this;
    }

    public Wrapper build() {
        return new Wrapper(id, address, port, offset);
    }

}
