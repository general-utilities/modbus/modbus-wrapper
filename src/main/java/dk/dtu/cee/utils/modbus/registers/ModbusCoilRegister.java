package dk.dtu.cee.utils.modbus.registers;

import dk.dtu.cee.utils.modbus.conversion.TwowayBitConverter;

public class ModbusCoilRegister<T> extends ModbusBitRegister<T> {

    public ModbusCoilRegister(int address, TwowayBitConverter<T> converter) {
        super(address, converter);
    }

    public boolean convert(T value) {
        return ((TwowayBitConverter)converter).convert(value);
    }
}
