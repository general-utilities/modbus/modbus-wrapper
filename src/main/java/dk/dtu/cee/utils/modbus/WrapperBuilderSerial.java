package dk.dtu.cee.utils.modbus;

import dk.dtu.cee.utils.modbus.parameters.*;

public class WrapperBuilderSerial extends WrapperBuilderBase {

    private final String port;

    private int baudRate = 19200;
    private Parity parityBit = Parity.Even;
    private StopBit stopBits = StopBit.One;
    private DataBit dataBits = DataBit.Eight;
    private Encoding encoding = Encoding.Rtu;
    private boolean echo = false;

    WrapperBuilderSerial(int id, String port) {
        super(id);
        this.port = port;
    }

    public WrapperBuilderSerial offset(Offset offset) {
        this.offset = offset;
        return this;
    }

    public WrapperBuilderSerial baudRate(int baudRate) {
        this.baudRate = baudRate;
        return this;
    }

    public WrapperBuilderSerial parityBit(Parity parityBit) {
        this.parityBit = parityBit;
        return this;
    }

    public WrapperBuilderSerial stopBits(StopBit stopBits) {
        this.stopBits = stopBits;
        return this;
    }

    public WrapperBuilderSerial dataBits(DataBit dataBits) {
        this.dataBits = dataBits;
        return this;
    }

    public WrapperBuilderSerial encoding(Encoding encoding) {
        this.encoding = encoding;
        return this;
    }

    public WrapperBuilderSerial echo(boolean echo) {
        this.echo = echo;
        return this;
    }

    public Wrapper build() {
        return new Wrapper(id, port, baudRate, parityBit, stopBits, dataBits, encoding, echo, offset);
    }

}
