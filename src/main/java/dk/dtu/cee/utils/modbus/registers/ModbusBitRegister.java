package dk.dtu.cee.utils.modbus.registers;

import dk.dtu.cee.utils.modbus.conversion.OnewayBitConverter;

public abstract class ModbusBitRegister<T> extends ModbusRegister<T> {

    protected final OnewayBitConverter<T> converter;

    public ModbusBitRegister(int address, OnewayBitConverter<T> converter) {
        super(address);
        this.converter = converter;
    }

    public T convert(boolean bit) {
        return converter.convert(bit);
    }

    @Override
    public int getSize() {
        return 1;
    }   // TODO In the future, this could (/should?) support multi-bit converters
}