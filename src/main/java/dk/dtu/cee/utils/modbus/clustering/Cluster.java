package dk.dtu.cee.utils.modbus.clustering;

import dk.dtu.cee.utils.modbus.registers.ModbusRegister;

public interface Cluster<T extends ModbusRegister> {
    int getAddress();
    int getSize();
}
