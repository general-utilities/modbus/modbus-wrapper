package dk.dtu.cee.utils.modbus.processing;

import dk.dtu.cee.utils.modbus.registers.ModbusRegister;

public abstract class ReadResponse<T> {
    protected final ModbusRegister<T> register;
    private final T value;

    public ReadResponse(ModbusRegister<T> register, T value) {
        this.register = register;
        this.value = value;
    }

    public ModbusRegister<T> getRegister() {
        return register;
    }

    public T getValue() {
        return value;
    }
}
