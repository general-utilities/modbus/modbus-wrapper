package dk.dtu.cee.utils.modbus.processing.coil;

import dk.dtu.cee.utils.modbus.processing.WriteRequest;
import dk.dtu.cee.utils.modbus.registers.ModbusCoilRegister;

public class WriteCoilRequest<T> extends WriteRequest<T> {
    public WriteCoilRequest(ModbusCoilRegister<T> register, T value) {
        super(register, value);
    }

    @Override
    public ModbusCoilRegister<T> getRegister() {
        return (ModbusCoilRegister)register;
    }
}
