package dk.dtu.cee.utils.modbus.parameters;

public enum Encoding {

    Ascii("ASCII"),
    Rtu("RTU"),
    Bin("BIN");

    private final String value;

    Encoding(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
