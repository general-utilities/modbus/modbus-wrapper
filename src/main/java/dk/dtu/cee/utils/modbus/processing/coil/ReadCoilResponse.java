package dk.dtu.cee.utils.modbus.processing.coil;

import dk.dtu.cee.utils.modbus.processing.ReadResponse;
import dk.dtu.cee.utils.modbus.registers.ModbusCoilRegister;

public class ReadCoilResponse<T> extends ReadResponse<T> {
    public ReadCoilResponse(ModbusCoilRegister<T> register, T value) {
        super(register, value);
    }

    @Override
    public ModbusCoilRegister<T> getRegister() {
        return (ModbusCoilRegister)register;
    }
}
