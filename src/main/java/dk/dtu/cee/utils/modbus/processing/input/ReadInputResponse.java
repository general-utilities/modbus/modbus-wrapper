package dk.dtu.cee.utils.modbus.processing.input;

import dk.dtu.cee.utils.modbus.processing.ReadResponse;
import dk.dtu.cee.utils.modbus.registers.ModbusRegister;

public class ReadInputResponse<T> extends ReadResponse<T> {
    public ReadInputResponse(ModbusRegister<T> register, T value) {
        super(register, value);
    }
}
