package dk.dtu.cee.utils.modbus.processing.discrete;

import dk.dtu.cee.utils.modbus.processing.ReadResponse;
import dk.dtu.cee.utils.modbus.registers.ModbusDiscreteRegister;

public class ReadDiscreteResponse<T> extends ReadResponse<T> {
    public ReadDiscreteResponse(ModbusDiscreteRegister<T> register, T value) {
        super(register, value);
    }

    @Override
    public ModbusDiscreteRegister<T> getRegister() {
        return (ModbusDiscreteRegister)register;
    }
}
