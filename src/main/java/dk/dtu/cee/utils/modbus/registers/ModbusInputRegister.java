package dk.dtu.cee.utils.modbus.registers;

import dk.dtu.cee.utils.modbus.conversion.OnewayByteConverter;

public class ModbusInputRegister<T> extends ModbusByteRegister<T> {

    public ModbusInputRegister(int address, OnewayByteConverter<T> converter) {
        super(address, converter);
    }

    public T convert(byte[] bytes) {
        return converter.convert(bytes);
    }
}
