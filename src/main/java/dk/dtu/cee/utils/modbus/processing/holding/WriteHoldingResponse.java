package dk.dtu.cee.utils.modbus.processing.holding;

import dk.dtu.cee.utils.modbus.processing.WriteResponse;
import dk.dtu.cee.utils.modbus.registers.ModbusHoldingRegister;

public class WriteHoldingResponse<T> extends WriteResponse<T> {
    public WriteHoldingResponse(ModbusHoldingRegister<T> register) {
        super(register);
    }

    @Override
    public ModbusHoldingRegister<T> getRegister() {
        return (ModbusHoldingRegister)register;
    }
}
