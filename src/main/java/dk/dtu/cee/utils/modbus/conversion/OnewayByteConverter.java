package dk.dtu.cee.utils.modbus.conversion;

public interface OnewayByteConverter<T> {
    int getSize();
    T convert(byte[] data);
}
