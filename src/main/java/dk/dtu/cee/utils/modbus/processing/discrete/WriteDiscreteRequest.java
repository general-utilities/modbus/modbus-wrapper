package dk.dtu.cee.utils.modbus.processing.discrete;

import dk.dtu.cee.utils.modbus.processing.WriteRequest;
import dk.dtu.cee.utils.modbus.registers.ModbusDiscreteRegister;

public class WriteDiscreteRequest<T> extends WriteRequest<T> {
    public WriteDiscreteRequest(ModbusDiscreteRegister<T> register, T value) {
        super(register, value);
    }

    @Override
    public ModbusDiscreteRegister<T> getRegister() {
        return (ModbusDiscreteRegister)register;
    }
}
