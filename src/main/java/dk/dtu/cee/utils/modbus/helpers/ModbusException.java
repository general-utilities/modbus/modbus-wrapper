package dk.dtu.cee.utils.modbus.helpers;

public enum ModbusException {

    ILLEGAL_FUNCTION(1),
    ILLEGAL_DATA_ADDRESS(2),
    ILLEGAL_DATA_VALUE(3),
    SLAVE_DEVICE_FAILURE(4),
    ACKNOWLEDGE(5),
    SLAVE_DEVICE_BUSY(6),
    NEGATIVE_ACKNOWLEDGE(7),
    MEMORY_PARITY_ERROR(8),
    GATEWAY_PATH_UNAVAILABLE(10),
    GATEWAY_TARGET_DEVICE_FAILED_TO_RESPOND(11),

    UNKNOWN_ERROR_CODE(0);

    private final int code;

    ModbusException(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

    public ModbusException parse(int code) {
        switch (code) {
            case 1:     return ModbusException.ILLEGAL_FUNCTION;
            case 2:     return ModbusException.ILLEGAL_DATA_ADDRESS;
            case 3:     return ModbusException.ILLEGAL_DATA_VALUE;
            case 4:     return ModbusException.SLAVE_DEVICE_FAILURE;
            case 5:     return ModbusException.ACKNOWLEDGE;
            case 6:     return ModbusException.SLAVE_DEVICE_BUSY;
            case 7:     return ModbusException.NEGATIVE_ACKNOWLEDGE;
            case 8:     return ModbusException.MEMORY_PARITY_ERROR;
            case 10:    return ModbusException.GATEWAY_PATH_UNAVAILABLE;
            case 11:    return ModbusException.GATEWAY_TARGET_DEVICE_FAILED_TO_RESPOND;

            default:    return ModbusException.UNKNOWN_ERROR_CODE;
        }
    }
}

