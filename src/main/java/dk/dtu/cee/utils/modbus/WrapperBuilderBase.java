package dk.dtu.cee.utils.modbus;

import dk.dtu.cee.utils.modbus.parameters.Offset;

public abstract class WrapperBuilderBase {

    protected final int id;
    protected Offset offset = Offset.Zero;

    WrapperBuilderBase(int id) {
        this.id = id;
    }
}
