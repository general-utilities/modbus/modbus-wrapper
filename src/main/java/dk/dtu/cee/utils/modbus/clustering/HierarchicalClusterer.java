package dk.dtu.cee.utils.modbus.clustering;

import dk.dtu.cee.utils.modbus.registers.ModbusRegister;

import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

public class HierarchicalClusterer<T extends ModbusRegister> {

    public List<Cluster<T>> cluster(List<T> registers) {
        List<Cluster<T>> clusters = registers.stream().map(reg -> new RegisterCluster<>(reg)).collect(Collectors.toList());

        Distance closest;

        while ((closest = findClosest(clusters)) != null) {
            // Remove closest cluster pair...
            clusters.remove(closest.getFirst());
            clusters.remove(closest.getSecond());

            // ...and replace with grouped cluster
            clusters.add(new GroupCluster(closest.getFirst(), closest.getSecond()));
        }

        return clusters;
    }

    private Distance findClosest(List<Cluster<T>> clusters) {
        Distance closest = null;

        ListIterator<Cluster<T>> outer = clusters.listIterator();

        while (outer.hasNext()) {
            Cluster outerCluster = outer.next();

            ListIterator<Cluster<T>> inner = clusters.listIterator(outer.nextIndex());

            while (inner.hasNext()) {
                Cluster innerCluster = inner.next();

                Distance distance = new Distance(outerCluster, innerCluster);

                if (closest == null || distance.getValue() < closest.getValue()) {
                    closest = (distance.getValue() <= 127) ? distance : closest;
                }
            }
        }

        return closest;
    }

    class Distance {
        private final Cluster first;
        private final Cluster second;
        private final int value;

        public Distance(Cluster first, Cluster second) {
            this.first = first;
            this.second = second;

            this.value = calculateDistance(first, second);
        }

        private int calculateDistance(Cluster c1, Cluster c2) {
            if (c1.getAddress() < c2.getAddress()) {
                return c2.getAddress() + c2.getSize() - c1.getAddress();
            } else {
                return c1.getAddress() + c1.getSize() - c2.getAddress();
            }
        }

        public Cluster getFirst() {
            return first;
        }

        public Cluster getSecond() {
            return second;
        }

        public double getValue() {
            return this.value;
        }
    }
}
