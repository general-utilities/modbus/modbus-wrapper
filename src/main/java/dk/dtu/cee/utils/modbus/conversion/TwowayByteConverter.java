package dk.dtu.cee.utils.modbus.conversion;

public interface TwowayByteConverter<T> extends OnewayByteConverter<T> {
    byte[] convert(T value);
}
