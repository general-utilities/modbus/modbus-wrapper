package dk.dtu.cee.utils.modbus.processing.input;

import dk.dtu.cee.utils.modbus.processing.WriteResponse;
import dk.dtu.cee.utils.modbus.registers.ModbusInputRegister;

public class WriteInputResponse<T> extends WriteResponse<T> {
    public WriteInputResponse(ModbusInputRegister<T> register) {
        super(register);
    }

    @Override
    public ModbusInputRegister<T> getRegister() {
        return (ModbusInputRegister)register;
    }
}
