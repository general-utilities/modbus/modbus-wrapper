package dk.dtu.cee.utils.modbus.registers;

import dk.dtu.cee.utils.modbus.conversion.OnewayBitConverter;

public class ModbusDiscreteRegister<T> extends ModbusBitRegister<T> {

    public ModbusDiscreteRegister(int address, OnewayBitConverter<T> converter) {
        super(address, converter);
    }
}
