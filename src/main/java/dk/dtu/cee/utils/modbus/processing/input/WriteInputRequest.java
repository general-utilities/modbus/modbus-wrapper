package dk.dtu.cee.utils.modbus.processing.input;

import dk.dtu.cee.utils.modbus.processing.WriteRequest;
import dk.dtu.cee.utils.modbus.registers.ModbusInputRegister;

public class WriteInputRequest<T> extends WriteRequest<T> {
    public WriteInputRequest(ModbusInputRegister<T> register, T value) {
        super(register, value);
    }

    @Override
    public ModbusInputRegister<T> getRegister() {
        return (ModbusInputRegister)register;
    }
}
