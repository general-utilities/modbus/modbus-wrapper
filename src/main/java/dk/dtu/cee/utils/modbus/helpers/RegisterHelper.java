package dk.dtu.cee.utils.modbus.helpers;

import net.wimpi.modbus.procimg.InputRegister;
import net.wimpi.modbus.procimg.Register;
import net.wimpi.modbus.procimg.SimpleRegister;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class RegisterHelper {
    public static byte[] registersToBytes(InputRegister registers[]) throws IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        for (InputRegister register : registers) {
            buffer.write(register.toBytes());
        }

        return buffer.toByteArray();
    }

    public static Register bytesToRegister(byte[] bytes) {
        return new SimpleRegister(bytes[0], bytes[1]);
    }
}
