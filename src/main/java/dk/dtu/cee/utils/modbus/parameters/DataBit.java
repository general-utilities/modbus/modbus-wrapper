package dk.dtu.cee.utils.modbus.parameters;

public enum DataBit {

    Seven(7),
    Eight(8);

    private final int value;

    DataBit(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
