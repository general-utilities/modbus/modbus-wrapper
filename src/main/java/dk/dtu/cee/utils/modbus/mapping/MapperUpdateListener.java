package dk.dtu.cee.utils.modbus.mapping;

import dk.dtu.cee.utils.modbus.registers.ModbusRegister;

/**
 * Created by abrpe on 2/20/17.
 */
public interface MapperUpdateListener {
	<T> void update(String key, ModbusRegister<T> register, T value);
}
