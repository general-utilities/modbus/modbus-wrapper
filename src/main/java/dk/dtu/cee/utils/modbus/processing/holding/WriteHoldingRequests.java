package dk.dtu.cee.utils.modbus.processing.holding;

import dk.dtu.cee.utils.modbus.processing.WriteRequest;
import dk.dtu.cee.utils.modbus.registers.ModbusHoldingRegister;

public class WriteHoldingRequests<T> extends WriteRequest<T> {
    public WriteHoldingRequests(ModbusHoldingRegister<T> register, T value) {
        super(register, value);
    }

    @Override
    public ModbusHoldingRegister<T> getRegister() {
        return (ModbusHoldingRegister)register;
    }
}
