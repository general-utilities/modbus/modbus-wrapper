package dk.dtu.cee.utils.modbus.processing;

import dk.dtu.cee.utils.modbus.registers.ModbusRegister;

public abstract class WriteResponse<T> {
    protected final ModbusRegister<T> register;

    public WriteResponse(ModbusRegister<T> register) {
        this.register = register;
    }

    public abstract ModbusRegister<T> getRegister();
}
