package dk.dtu.cee.utils.modbus;

public class WrapperBuilder {

    private final int id;

    public WrapperBuilder(int id) {
        this.id = id;
    }

    public WrapperBuilderSerial serial(String port) {
        return new WrapperBuilderSerial(id, port);
    }

    public WrapperBuilderTCP tcp(String address) {
        return new WrapperBuilderTCP(id, address);
    }

}
