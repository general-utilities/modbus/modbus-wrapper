package dk.dtu.cee.utils.modbus.processing.coil;

import dk.dtu.cee.utils.modbus.processing.ReadRequest;
import dk.dtu.cee.utils.modbus.processing.ReadResponse;
import dk.dtu.cee.utils.modbus.registers.ModbusCoilRegister;

public class ReadCoilRequest<T> extends ReadRequest<T> {
    public ReadCoilRequest(ModbusCoilRegister<T> register) {
        super(register);
    }

    @Override
    public ModbusCoilRegister<T> getRegister() {
        return (ModbusCoilRegister) register;
    }

    public ReadCoilResponse<T> createResponse(boolean bit) {
        ModbusCoilRegister<T> reg = (ModbusCoilRegister<T>)register;

        return new ReadCoilResponse<>(reg, reg.convert(bit));
    }

}
