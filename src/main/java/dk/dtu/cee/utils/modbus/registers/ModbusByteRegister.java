package dk.dtu.cee.utils.modbus.registers;

import dk.dtu.cee.utils.modbus.conversion.OnewayByteConverter;

public abstract class ModbusByteRegister<T> extends ModbusRegister<T> {

    protected final OnewayByteConverter<T> converter;

    public ModbusByteRegister(int address, OnewayByteConverter<T> converter) {
        super(address);
        this.converter = converter;
    }

    public T convert(byte[] bytes) {
        return converter.convert(bytes);
    }

    @Override
    public int getSize() {
        return converter.getSize();
    }

}