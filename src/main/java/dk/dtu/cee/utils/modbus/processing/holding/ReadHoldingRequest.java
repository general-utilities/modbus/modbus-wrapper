package dk.dtu.cee.utils.modbus.processing.holding;

import dk.dtu.cee.utils.modbus.processing.ReadRequest;
import dk.dtu.cee.utils.modbus.processing.input.ReadInputResponse;
import dk.dtu.cee.utils.modbus.registers.ModbusHoldingRegister;
import dk.dtu.cee.utils.modbus.registers.ModbusInputRegister;

public class ReadHoldingRequest<T> extends ReadRequest<T> {
    public ReadHoldingRequest(ModbusHoldingRegister<T> register) {
        super(register);
    }

    @Override
    public ModbusHoldingRegister<T> getRegister() {
        return (ModbusHoldingRegister)register;
    }

    public ReadHoldingResponse createResponse(byte[] bytes) {
        ModbusHoldingRegister reg = (ModbusHoldingRegister)register;

        return new ReadHoldingResponse<>(reg, reg.convert(bytes));
    }
}
