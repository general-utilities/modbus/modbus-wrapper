package dk.dtu.cee.utils.modbus.registers;

public abstract class ModbusRegister<T> {
    private final int address;

    public ModbusRegister(int address) {
        this.address = address;
    }

    public int getAddress() {
        return this.address;
    }

    public abstract int getSize();

    @Override
    public String toString() {
        return String.format("%s(%d)", this.getClass().getSimpleName(), this.address);
    }
}