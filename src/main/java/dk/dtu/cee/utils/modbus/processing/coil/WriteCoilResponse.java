package dk.dtu.cee.utils.modbus.processing.coil;

import dk.dtu.cee.utils.modbus.processing.WriteResponse;
import dk.dtu.cee.utils.modbus.registers.ModbusCoilRegister;

public class WriteCoilResponse<T> extends WriteResponse<T> {
    public WriteCoilResponse(ModbusCoilRegister<T> register) {
        super(register);
    }

    @Override
    public ModbusCoilRegister<T> getRegister() {
        return (ModbusCoilRegister)register;
    }
}
