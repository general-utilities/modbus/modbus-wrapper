package dk.dtu.cee.utils.modbus.processing.holding;

import dk.dtu.cee.utils.modbus.processing.ReadRequests;

public class ReadHoldingRequests extends ReadRequests {
    public ReadHoldingRequests(ReadHoldingRequest<?>...requests) {
        super(requests);
    }

    @Override
    public ReadHoldingRequest<?>[] getRequests() {
        return (ReadHoldingRequest<?>[])requests;
    }
}
