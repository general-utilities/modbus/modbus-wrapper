package dk.dtu.cee.utils.modbus.clustering;

import dk.dtu.cee.utils.modbus.registers.ModbusRegister;

public class RegisterCluster<T extends ModbusRegister> implements Cluster<T> {
    private final T register;

    public RegisterCluster(T register) {
        this.register = register;
    }

    public T getRegister() {
        return this.register;
    }

    @Override
    public int getAddress() {
        return this.register.getAddress();
    }

    @Override
    public int getSize() {
        return this.register.getSize();
    }
}
