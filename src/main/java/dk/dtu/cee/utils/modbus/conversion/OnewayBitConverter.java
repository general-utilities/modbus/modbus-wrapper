package dk.dtu.cee.utils.modbus.conversion;

public interface OnewayBitConverter<T> {
    T convert(boolean bit);
}
