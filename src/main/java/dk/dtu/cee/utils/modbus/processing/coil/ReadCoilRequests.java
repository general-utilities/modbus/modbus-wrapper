package dk.dtu.cee.utils.modbus.processing.coil;

import dk.dtu.cee.utils.modbus.processing.ReadRequests;

public class ReadCoilRequests extends ReadRequests {
    public ReadCoilRequests(ReadCoilRequest<?>...requests) {
        super(requests);
    }

    @Override
    public ReadCoilRequest<?>[] getRequests() {
        return (ReadCoilRequest<?>[])requests;
    }
}
