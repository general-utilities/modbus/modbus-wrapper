package dk.dtu.cee.utils.modbus.clustering;

import dk.dtu.cee.utils.modbus.registers.ModbusRegister;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ClusterHelper {

    public static <T extends ModbusRegister> List<T> getRegisters(Cluster<T> cluster) {

        if (cluster instanceof RegisterCluster) {
            return Arrays.asList(((RegisterCluster<T>)cluster).getRegister());
        } else {
            GroupCluster group = (GroupCluster)cluster;

            List<T> left = getRegisters(group.getLeft());
            List<T> right = getRegisters(group.getRight());

            List<T> joined = new ArrayList<>();
            joined.addAll(left);
            joined.addAll(right);

            return joined;
        }
    }
}
