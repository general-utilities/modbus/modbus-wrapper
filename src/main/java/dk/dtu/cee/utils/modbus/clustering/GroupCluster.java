package dk.dtu.cee.utils.modbus.clustering;

import dk.dtu.cee.utils.modbus.registers.ModbusRegister;

public class GroupCluster<T extends ModbusRegister> implements Cluster<T> {
    private final Cluster left;
    private final Cluster right;

    private final int addr;
    private final int size;

    public GroupCluster(Cluster<T> left, Cluster<T> right) {
        if (left.getAddress() < right.getAddress()) {
            this.left = left;
            this.right = right;
            this.addr = left.getAddress();
            this.size = right.getAddress() + right.getSize() - left.getAddress();
        } else {
            this.left = right;
            this.right = left;
            this.addr = right.getAddress();
            this.size = left.getAddress() + left.getSize() - right.getAddress();
        }
    }

    public Cluster getLeft() {
        return left;
    }

    public Cluster getRight() {
        return right;
    }

    @Override
    public int getAddress() {
        return this.addr;
    }

    @Override
    public int getSize() {
        return this.size;
    }
}
