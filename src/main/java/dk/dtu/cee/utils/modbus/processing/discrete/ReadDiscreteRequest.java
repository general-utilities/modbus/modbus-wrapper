package dk.dtu.cee.utils.modbus.processing.discrete;

import dk.dtu.cee.utils.modbus.processing.ReadRequest;
import dk.dtu.cee.utils.modbus.processing.coil.ReadCoilResponse;
import dk.dtu.cee.utils.modbus.registers.ModbusCoilRegister;
import dk.dtu.cee.utils.modbus.registers.ModbusDiscreteRegister;

public class ReadDiscreteRequest<T> extends ReadRequest<T> {
    public ReadDiscreteRequest(ModbusDiscreteRegister<T> register) {
        super(register);
    }

    @Override
    public ModbusDiscreteRegister<T> getRegister() {
        return (ModbusDiscreteRegister)register;
    }

    public ReadDiscreteResponse<T> createResponse(boolean bit) {
        ModbusDiscreteRegister<T> reg = (ModbusDiscreteRegister<T>)register;

        return new ReadDiscreteResponse<>(reg, reg.convert(bit));
    }
}
