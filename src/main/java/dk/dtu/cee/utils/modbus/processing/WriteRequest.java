package dk.dtu.cee.utils.modbus.processing;

import dk.dtu.cee.utils.modbus.registers.ModbusRegister;

public abstract class WriteRequest<T> {
    protected final ModbusRegister<T> register;
    private final T value;

    public WriteRequest(ModbusRegister<T> register, T value) {
        this.register = register;
        this.value = value;
    }

    public abstract ModbusRegister<T> getRegister();

    public T getValue() {
        return value;
    }
}
