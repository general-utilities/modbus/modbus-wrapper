package dk.dtu.cee.utils.modbus.registers;

import dk.dtu.cee.utils.modbus.conversion.TwowayByteConverter;

public class ModbusHoldingRegister<T> extends ModbusByteRegister<T> {

    public ModbusHoldingRegister(int address, TwowayByteConverter<T> converter) {
        super(address, converter);
    }

    public byte[] convert(T value) {
        return ((TwowayByteConverter)converter).convert(value);
    }
}
