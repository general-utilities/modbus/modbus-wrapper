package dk.dtu.cee.utils.modbus.processing.coil;

import dk.dtu.cee.utils.modbus.processing.WriteRequest;
import dk.dtu.cee.utils.modbus.registers.ModbusCoilRegister;

public class WriteCoilRequests<T> extends WriteRequest<T> {
    public WriteCoilRequests(ModbusCoilRegister<T> register, T value) {
        super(register, value);
    }

    @Override
    public ModbusCoilRegister<T> getRegister() {
        return (ModbusCoilRegister)register;
    }
}
