package dk.dtu.cee.utils.modbus.processing;

import dk.dtu.cee.utils.modbus.registers.ModbusRegister;

public abstract class ReadRequest<T> {
    protected final ModbusRegister<T> register;

    public ReadRequest(ModbusRegister<T> register) {
        this.register = register;
    }

    public abstract ModbusRegister<T> getRegister();

    public T getValue() { return null; }
}
