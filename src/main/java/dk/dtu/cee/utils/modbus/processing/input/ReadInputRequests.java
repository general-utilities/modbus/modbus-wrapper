package dk.dtu.cee.utils.modbus.processing.input;

import dk.dtu.cee.utils.modbus.processing.ReadRequests;

public class ReadInputRequests extends ReadRequests {
    public ReadInputRequests(ReadInputRequest<?>...requests) {
        super(requests);
    }

    @Override
    public ReadInputRequest<?>[] getRequests() {
        return (ReadInputRequest<?>[])requests;
    }
}
