package dk.dtu.cee.utils.modbus.processing;

import dk.dtu.cee.utils.modbus.registers.ModbusRegister;

public abstract class ReadRequests {

    protected final ReadRequest<?>[] requests;
    protected final int address;
    protected final int length;

    public ReadRequests(ReadRequest<?>... requests) {
        this.requests = requests;

        ModbusRegister<?> first;
        ModbusRegister<?> last;

        first = last = (requests.length == 0) ? null : requests[0].getRegister();

        for (ReadRequest<?> request : requests) {
            first = (request.getRegister().getAddress() < first.getAddress()) ? request.getRegister() : first;
            last = (request.getRegister().getAddress() > last.getAddress()) ? request.getRegister() : last;
        }

        this.address = (first != null) ? first.getAddress() : 0;
        this.length = (first != null) ? last.getAddress() - first.getAddress() + last.getSize() : 0;
    }

    public abstract ReadRequest<?>[] getRequests();

    public int getAddress() {
        return address;
    }

    public int getLength() {
        return length;
    }
}
