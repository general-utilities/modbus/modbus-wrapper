package dk.dtu.cee.utils.modbus;

import dk.dtu.cee.utils.modbus.helpers.RegisterHelper;
import dk.dtu.cee.utils.modbus.parameters.*;
import dk.dtu.cee.utils.modbus.processing.coil.*;
import dk.dtu.cee.utils.modbus.processing.coil.WriteCoilRequest;
import dk.dtu.cee.utils.modbus.processing.coil.WriteCoilResponse;
import dk.dtu.cee.utils.modbus.processing.discrete.ReadDiscreteRequest;
import dk.dtu.cee.utils.modbus.processing.discrete.ReadDiscreteRequests;
import dk.dtu.cee.utils.modbus.processing.discrete.ReadDiscreteResponse;
import dk.dtu.cee.utils.modbus.processing.holding.*;
import dk.dtu.cee.utils.modbus.processing.input.ReadInputRequest;
import dk.dtu.cee.utils.modbus.processing.input.ReadInputRequests;
import dk.dtu.cee.utils.modbus.processing.input.ReadInputResponse;
import net.wimpi.modbus.ModbusException;
import net.wimpi.modbus.io.ModbusSerialTransaction;
import net.wimpi.modbus.io.ModbusTCPTransaction;
import net.wimpi.modbus.io.ModbusTransaction;
import net.wimpi.modbus.msg.*;
import net.wimpi.modbus.net.SerialConnection;
import net.wimpi.modbus.net.TCPMasterConnection;
import net.wimpi.modbus.procimg.Register;
import net.wimpi.modbus.util.SerialParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Wrapper {
    private static final Logger logger = LoggerFactory.getLogger(Wrapper.class);

    private final int TRANSACTION_GRADE_PERIOD = 5;
    private final int RETRIES = 3;

    private final int unitID;
    private final Offset regOffset;

    private TCPMasterConnection conTCP;
    private SerialConnection conSerial;

    private final ExecutorService executor = Executors.newSingleThreadExecutor();

    Wrapper(int unitID, String serialPort, int baudRate, Parity parityBit, StopBit stopBits, DataBit dataBits, Encoding encoding, boolean enableEcho, Offset regOffset) {
        this.unitID = unitID;
        this.regOffset = regOffset;

        SerialParameters params = new SerialParameters();

        params.setPortName(serialPort);
        params.setBaudRate(baudRate);
        params.setParity(parityBit.getValue());
        params.setStopbits(stopBits.getValue());
        params.setDatabits(dataBits.getValue());
        params.setEncoding(encoding.getValue());
        params.setEcho(enableEcho);

        System.setProperty("gnu.io.rxtx.SerialPorts", serialPort);

        conSerial = new SerialConnection(params);

        try {
            conSerial.open();
        } catch (Exception e) {
            logger.error("Error connecting to serial device (exception: {})", e.getMessage());
        }
    }

    Wrapper(int unitID, String address, int port, Offset regOffset) {
        this.unitID = unitID;
        this.regOffset = regOffset;

        try {
            InetAddress addr = InetAddress.getByName(address);

            conTCP = new TCPMasterConnection(addr);
            conTCP.setPort(port);
            conTCP.setTimeout(2500);
        } catch (UnknownHostException e) {
            logger.error("Error connecting to TCP device (exception: {})", e.getMessage());
        }
    }

    /*** Read single ***/

    public <T> ReadCoilResponse<T> read(ReadCoilRequest<T> request) {
        Future<ReadCoilResponse<T>> future = readAsync(request);

        try {
            return future.get();
        } catch (Exception e) {
            return null;
        }
    }


    public <T> Future<ReadCoilResponse<T>> readAsync(ReadCoilRequest<T> request) {
        return executor.submit(() -> {
            ReadCoilsRequest req = new ReadCoilsRequest(request.getRegister().getAddress(), 1);

            ModbusResponse resRaw = execute(req);

            if (resRaw instanceof ExceptionResponse) {
                ExceptionResponse e = (ExceptionResponse) resRaw;
                throw new Exception(String.format("Exception (code: %d)", e.getExceptionCode()));
            } else {
                ReadCoilsResponse res = (ReadCoilsResponse) resRaw;

                boolean bit = res.getCoils().getBit(0);

                T raw = request.getRegister().convert(bit);
                ReadCoilResponse<T> response = new ReadCoilResponse<>(request.getRegister(), raw);

                return response;
            }
        });
    }

    public <T> ReadDiscreteResponse<T> read(ReadDiscreteRequest<T> request) {
        Future<ReadDiscreteResponse<T>> future = readAsync(request);

        try {
            return future.get();
        } catch (Exception e) {
            return null;
        }
    }

    public <T> Future<ReadDiscreteResponse<T>> readAsync(ReadDiscreteRequest<T> request) {
        return executor.submit(() -> {
            ReadInputDiscretesRequest req = new ReadInputDiscretesRequest(request.getRegister().getAddress(), 1);

            ModbusResponse resRaw = execute(req);

            if (resRaw instanceof ExceptionResponse) {
                ExceptionResponse e = (ExceptionResponse) resRaw;
                throw new Exception(String.format("Exception (code: %d)", e.getExceptionCode()));
            } else {
                ReadInputDiscretesResponse res = (ReadInputDiscretesResponse) resRaw;

                boolean bit = res.getDiscretes().getBit(0);

                T raw = request.getRegister().convert(bit);
                ReadDiscreteResponse<T> response = new ReadDiscreteResponse<>(request.getRegister(), raw);

                return response;
            }
        });
    }

    public <T> ReadInputResponse<T> read(ReadInputRequest<T> request) {
        Future<ReadInputResponse<T>> future = readAsync(request);

        try {
            return future.get();
        } catch (Exception e) {
            return null;
        }
    }

    public <T> Future<ReadInputResponse<T>> readAsync(ReadInputRequest<T> request) {
        return executor.submit(() -> {
            ReadInputRegistersRequest req = new ReadInputRegistersRequest(
                    request.getRegister().getAddress() - this.regOffset.getValue(),
                    request.getRegister().getSize());

            ModbusResponse resRaw = execute(req);

            if (resRaw instanceof ExceptionResponse) {
                ExceptionResponse e = (ExceptionResponse) resRaw;
                throw new Exception(String.format("Exception (code: %d)", e.getExceptionCode()));
            } else {
                ReadInputRegistersResponse res = (ReadInputRegistersResponse) resRaw;

                byte[] buffer = RegisterHelper.registersToBytes(res.getRegisters());

                T raw = request.getRegister().convert(buffer);
                ReadInputResponse<T> response = new ReadInputResponse<>(request.getRegister(), raw);

                return response;
            }
        });
    }

    public <T> ReadHoldingResponse<T> read(ReadHoldingRequest<T> request) {
        Future<ReadHoldingResponse<T>> future = readAsync(request);

        try {
            return future.get();
        } catch (Exception e) {
            return null;
        }
    }

    public <T> Future<ReadHoldingResponse<T>> readAsync(ReadHoldingRequest<T> request) {
        return executor.submit(() -> {
            ReadMultipleRegistersRequest req = new ReadMultipleRegistersRequest(
                    request.getRegister().getAddress() - this.regOffset.getValue(),
                    request.getRegister().getSize());

            ModbusResponse resRaw = execute(req);

            if (resRaw instanceof ExceptionResponse) {
                ExceptionResponse e = (ExceptionResponse) resRaw;
                throw new Exception(String.format("Exception (code: %d)", e.getExceptionCode()));
            } else {
                ReadMultipleRegistersResponse res = (ReadMultipleRegistersResponse) resRaw;

                byte[] buffer = RegisterHelper.registersToBytes(res.getRegisters());

                T raw = request.getRegister().convert(buffer);
                ReadHoldingResponse<T> response = new ReadHoldingResponse<>(request.getRegister(), raw);

                return response;
            }
        });
    }

    /*** Read group ***/

    public List<ReadCoilResponse<?>> read(ReadCoilRequests requests) {
        Future<List<ReadCoilResponse<?>>> future = readAsync(requests);

        try {
            return future.get();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public Future<List<ReadCoilResponse<?>>> readAsync(ReadCoilRequests requests) {
        return executor.submit(() -> {
            ReadCoilsRequest req = new ReadCoilsRequest(
                    requests.getAddress() - this.regOffset.getValue(),
                    requests.getLength()
            );

            ModbusResponse resRaw = execute(req);

            if (resRaw instanceof ExceptionResponse) {
                ExceptionResponse e = (ExceptionResponse) resRaw;
                throw new Exception(String.format("Exception (code: %d)", e.getExceptionCode()));
            } else {
                ReadCoilsResponse res = (ReadCoilsResponse) resRaw;

                List<ReadCoilResponse<?>> response = new ArrayList<>();

                for (ReadCoilRequest<?> request : requests.getRequests()) {
                    int offset = request.getRegister().getAddress() - requests.getAddress();

                    boolean bit = res.getCoils().getBit(offset);

                    response.add(request.createResponse(bit));
                }

                return response;
            }
        });
    }

    public List<ReadDiscreteResponse<?>> read(ReadDiscreteRequests requests) {
        Future<List<ReadDiscreteResponse<?>>> future = readAsync(requests);

        try {
            return future.get();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public Future<List<ReadDiscreteResponse<?>>> readAsync(ReadDiscreteRequests requests) {
        return executor.submit(() -> {
            ReadInputDiscretesRequest req = new ReadInputDiscretesRequest(
                    requests.getAddress() - this.regOffset.getValue(),
                    requests.getLength()
            );

            ModbusResponse resRaw = execute(req);

            if (resRaw instanceof ExceptionResponse) {
                ExceptionResponse e = (ExceptionResponse) resRaw;
                throw new Exception(String.format("Exception (code: %d)", e.getExceptionCode()));
            } else {
                ReadInputDiscretesResponse res = (ReadInputDiscretesResponse) resRaw;

                List<ReadDiscreteResponse<?>> response = new ArrayList<>();

                for (ReadDiscreteRequest<?> request : requests.getRequests()) {
                    int offset = request.getRegister().getAddress() - requests.getAddress();

                    boolean bit = res.getDiscretes().getBit(offset);

                    response.add(request.createResponse(bit));
                }

                return response;
            }
        });
    }

    public List<ReadInputResponse<?>> read(ReadInputRequests requests) {
        Future<List<ReadInputResponse<?>>> future = readAsync(requests);

        try {
            return future.get();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public Future<List<ReadInputResponse<?>>> readAsync(ReadInputRequests requests) {
        return executor.submit(() -> {
            ReadInputRegistersRequest req = new ReadInputRegistersRequest(
                    requests.getAddress() - this.regOffset.getValue(),
                    requests.getLength()
            );

            ModbusResponse resRaw = execute(req);

            if (resRaw instanceof ExceptionResponse) {
                ExceptionResponse e = (ExceptionResponse) resRaw;
                throw new Exception(String.format("Exception (code: %d)", e.getExceptionCode()));
            } else {
                ReadInputRegistersResponse res = (ReadInputRegistersResponse) resRaw;

                List<ReadInputResponse<?>> response = new ArrayList<>();

                for (ReadInputRequest<?> request : requests.getRequests()) {
                    int length = request.getRegister().getSize();
                    int offset = request.getRegister().getAddress() - requests.getAddress();

                    Register[] regs = new Register[length];
                    System.arraycopy(res.getRegisters(), offset, regs, 0, regs.length);
                    byte[] bytes = RegisterHelper.registersToBytes(regs);

                    response.add(request.createResponse(bytes));
                }

                return response;
            }
        });
    }

    public List<ReadHoldingResponse<?>> read(ReadHoldingRequests requests) {
        Future<List<ReadHoldingResponse<?>>> future = readAsync(requests);

        try {
            return future.get();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public Future<List<ReadHoldingResponse<?>>> readAsync(ReadHoldingRequests requests) {
        return executor.submit(() -> {
            ReadMultipleRegistersRequest req = new ReadMultipleRegistersRequest(
                    requests.getAddress() - this.regOffset.getValue(),
                    requests.getLength());

            ModbusResponse resRaw = execute(req);

            if (resRaw instanceof ExceptionResponse) {
                ExceptionResponse e = (ExceptionResponse) resRaw;
                throw new Exception(String.format("Exception (code: %d)", e.getExceptionCode()));
            } else {
                ReadMultipleRegistersResponse res = (ReadMultipleRegistersResponse) resRaw;

                List<ReadHoldingResponse<?>> response = new ArrayList<>();

                for (ReadHoldingRequest<?> request : requests.getRequests()) {
                    int length = request.getRegister().getSize();
                    int offset = request.getRegister().getAddress() - requests.getAddress();

                    Register[] regs = new Register[length];
                    System.arraycopy(res.getRegisters(), offset, regs, 0, regs.length);
                    byte[] bytes = RegisterHelper.registersToBytes(regs);

                    response.add(request.createResponse(bytes));
                }

                return response;
            }
        });
    }

    /*** Write single ***/

    public <T> WriteCoilResponse<T> write(WriteCoilRequest<T> request) {
        Future<WriteCoilResponse<T>> future = writeAsync(request);

        try {
            return future.get();
        } catch (Exception e) {
            return null;
        }
    }

    public <T> Future<WriteCoilResponse<T>> writeAsync(WriteCoilRequest<T> request) {
        return executor.submit(() -> {
            boolean bit = request.getRegister().convert(request.getValue());

            net.wimpi.modbus.msg.WriteCoilRequest req = new net.wimpi.modbus.msg.WriteCoilRequest(
                    request.getRegister().getAddress() - this.regOffset.getValue(),
                    bit);

            ModbusResponse res = execute(req);

            if (res instanceof ExceptionResponse) {
                ExceptionResponse e = (ExceptionResponse) res;
                throw new Exception(String.format("Exception (code: %d)", e.getExceptionCode()));
            } else {
                WriteCoilResponse<T> response = new WriteCoilResponse<>(request.getRegister());

                return response;
            }
        });
    }

    public <T> WriteHoldingResponse<T> write(WriteHoldingRequest<T> request) {
        Future<WriteHoldingResponse<T>> future = writeAsync(request);

        try {
            return future.get();
        } catch (Exception e) {
            return null;
        }
    }

    public <T> Future<WriteHoldingResponse<T>> writeAsync(WriteHoldingRequest<T> request) {
        return executor.submit(() -> {
            Register register = RegisterHelper.bytesToRegister(request.getRegister().convert(request.getValue()));

            WriteSingleRegisterRequest req = new WriteSingleRegisterRequest(
                    request.getRegister().getAddress() - this.regOffset.getValue(),
                    register
            );

            ModbusResponse res = execute(req);

            if (res instanceof ExceptionResponse) {
                ExceptionResponse e = (ExceptionResponse) res;
                throw new Exception(String.format("Exception (code: %d)", e.getExceptionCode()));
            } else {
                WriteHoldingResponse<T> response = new WriteHoldingResponse<>(request.getRegister());

                return response;
            }
        });
    }

    /*** Write group ***/

    public List<WriteCoilResponse<?>> write(WriteCoilRequests request) {
        Future<List<WriteCoilResponse<?>>> future = writeAsync(request);

        try {
            return future.get();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public Future<List<WriteCoilResponse<?>>> writeAsync(WriteCoilRequests requests) {
        throw new NotImplementedException();
    }

    public List<WriteHoldingResponse<?>> write(WriteHoldingRequests request) {
        Future<List<WriteHoldingResponse<?>>> future = writeAsync(request);

        try {
            return future.get();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public Future<List<WriteHoldingResponse<?>>> writeAsync(WriteHoldingRequests requests) {
        throw new NotImplementedException();
    }

    /*** General ***/

    private ModbusTransaction getTransaction() {

        if (conSerial != null) {

            ModbusSerialTransaction transaction = new ModbusSerialTransaction(conSerial);

            transaction.setRetries(3);
            transaction.setCheckingValidity(true);
            transaction.setTransDelayMS(50); // TODO: Figure out what this does

            return transaction;

        }

        if (conTCP != null) {
            ModbusTCPTransaction transaction = new ModbusTCPTransaction(conTCP);

            transaction.setReconnecting(true);
            transaction.setRetries(3);
            transaction.setCheckingValidity(true);

            return transaction;
        }

        return null;
    }

    private ModbusResponse execute(ModbusRequest request) throws ModbusException, InterruptedException {
        request.setUnitID(unitID);

        ModbusTransaction transaction = getTransaction();

        if (transaction != null) {
            transaction.setRetries(RETRIES);
            transaction.setRequest(request);
            transaction.setCheckingValidity(true);

            // TODO Perform better checking on requests (e.g. that multi-register range <= 127)
            transaction.execute();

            Thread.sleep(TRANSACTION_GRADE_PERIOD);

            return transaction.getResponse();
        } else {
            throw new ModbusException("No connection available to create transaction");
        }
    }
}
