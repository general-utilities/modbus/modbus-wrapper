package dk.dtu.cee.utils.modbus.parameters;

/**
 * Created by abrpe on 2/18/17.
 */
public enum Offset {
    Zero(0),
    One(1);

    private final int value;

    Offset(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
