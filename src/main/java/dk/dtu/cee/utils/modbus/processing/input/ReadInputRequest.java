package dk.dtu.cee.utils.modbus.processing.input;

import dk.dtu.cee.utils.modbus.processing.ReadRequest;
import dk.dtu.cee.utils.modbus.registers.ModbusInputRegister;

public class ReadInputRequest<T> extends ReadRequest<T> {
    public ReadInputRequest(ModbusInputRegister<T> register) {
        super(register);
    }

    @Override
    public ModbusInputRegister<T> getRegister() {
        return (ModbusInputRegister)register;
    }

    public ReadInputResponse createResponse(byte[] bytes) {
        ModbusInputRegister reg = (ModbusInputRegister)register;

        return new ReadInputResponse<>(reg, reg.convert(bytes));
    }
}
