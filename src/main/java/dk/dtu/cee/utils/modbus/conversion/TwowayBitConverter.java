package dk.dtu.cee.utils.modbus.conversion;

public interface TwowayBitConverter<T> extends OnewayBitConverter<T> {
    boolean convert(T value);
}
