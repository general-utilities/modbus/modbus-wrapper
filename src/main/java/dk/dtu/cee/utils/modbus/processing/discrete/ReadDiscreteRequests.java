package dk.dtu.cee.utils.modbus.processing.discrete;

import dk.dtu.cee.utils.modbus.processing.ReadRequests;

public class ReadDiscreteRequests extends ReadRequests {
    public ReadDiscreteRequests(ReadDiscreteRequest<?>...requests) {
        super(requests);
    }


    @Override
    public ReadDiscreteRequest<?>[] getRequests() {
        return (ReadDiscreteRequest<?>[])requests;
    }
}
