package dk.dtu.cee.utils.modbus.parameters;

public enum Parity {

    None(0),
    Odd(1),
    Even(2),
    Mark(3),
    Space(4);

    private final int value;

    Parity(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
