package dk.dtu.cee.utils.modbus.mapping;

import dk.dtu.cee.utils.modbus.Wrapper;
import dk.dtu.cee.utils.modbus.clustering.Cluster;
import dk.dtu.cee.utils.modbus.clustering.ClusterHelper;
import dk.dtu.cee.utils.modbus.clustering.HierarchicalClusterer;
import dk.dtu.cee.utils.modbus.processing.ReadResponse;
import dk.dtu.cee.utils.modbus.processing.WriteResponse;
import dk.dtu.cee.utils.modbus.processing.coil.*;
import dk.dtu.cee.utils.modbus.processing.discrete.ReadDiscreteRequest;
import dk.dtu.cee.utils.modbus.processing.discrete.ReadDiscreteRequests;
import dk.dtu.cee.utils.modbus.processing.discrete.ReadDiscreteResponse;
import dk.dtu.cee.utils.modbus.processing.holding.*;
import dk.dtu.cee.utils.modbus.processing.input.ReadInputRequest;
import dk.dtu.cee.utils.modbus.processing.input.ReadInputRequests;
import dk.dtu.cee.utils.modbus.processing.input.ReadInputResponse;
import dk.dtu.cee.utils.modbus.registers.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by abrpe on 2/20/17.
 */
public class Mapper {

    private HashMap<String, ModbusRegister<?>> keyMap = new HashMap<>();
    private HashMap<ModbusRegister<?>, String> regMap = new HashMap<>();

    private List<MapperUpdateListener> listeners = new ArrayList<>();

    private List<ReadCoilRequests> coilRequests = new ArrayList<>();
    private List<ReadDiscreteRequests> discreteRequests = new ArrayList<>();
    private List<ReadInputRequests> inputRequests = new ArrayList<>();
    private List<ReadHoldingRequests> holdingRequests = new ArrayList<>();

    private final Wrapper wrapper;

    public Mapper(Wrapper wrapper) {
        this.wrapper = wrapper;
    }

    public <T extends ModbusRegister<?>> T add(String key, T reg) {
        if (keyMap.containsKey(key)) {
            throw new IllegalArgumentException(String.format("Key [%s] already mapped", key));
        }

        if (regMap.containsKey(reg)) {
            throw new IllegalArgumentException(String.format("Register [%s] already mapped", reg));
        }

        keyMap.put(key, reg);
        regMap.put(reg, key);

        return reg;
    }

    public void prepare() {
        // Filter requests (TODO Should be done when originally added!)
        List<ModbusCoilRegister> coilRegisters = keyMap.values().stream().filter(reg -> reg instanceof ModbusCoilRegister<?>).map(reg -> (ModbusCoilRegister) reg).collect(Collectors.toList());
        List<ModbusDiscreteRegister> discreteRegisters = keyMap.values().stream().filter(reg -> reg instanceof ModbusDiscreteRegister<?>).map(reg -> (ModbusDiscreteRegister) reg).collect(Collectors.toList());
        List<ModbusInputRegister> inputRegisters = keyMap.values().stream().filter(reg -> reg instanceof ModbusInputRegister<?>).map(reg -> (ModbusInputRegister) reg).collect(Collectors.toList());
        List<ModbusHoldingRegister> holdingRegisters = keyMap.values().stream().filter(reg -> reg instanceof ModbusHoldingRegister<?>).map(reg -> (ModbusHoldingRegister) reg).collect(Collectors.toList());

        // Cluster registers
        List<Cluster<ModbusCoilRegister>> coilRegisterClusters = new HierarchicalClusterer<ModbusCoilRegister>().cluster(coilRegisters);
        List<Cluster<ModbusDiscreteRegister>> discreteRegisterClusters = new HierarchicalClusterer<ModbusDiscreteRegister>().cluster(discreteRegisters);
        List<Cluster<ModbusInputRegister>> inputRegisterClusters = new HierarchicalClusterer<ModbusInputRegister>().cluster(inputRegisters);
        List<Cluster<ModbusHoldingRegister>> holdingRegisterClusters = new HierarchicalClusterer<ModbusHoldingRegister>().cluster(holdingRegisters);

        // Extract register lists
        List<List<ModbusCoilRegister>> coilLists = coilRegisterClusters.stream().map(cluster -> ClusterHelper.getRegisters(cluster)).collect(Collectors.toList());
        List<List<ModbusDiscreteRegister>> discreteLists = discreteRegisterClusters.stream().map(cluster -> ClusterHelper.getRegisters(cluster)).collect(Collectors.toList());
        List<List<ModbusInputRegister>> inputLists = inputRegisterClusters.stream().map(cluster -> ClusterHelper.getRegisters(cluster)).collect(Collectors.toList());
        List<List<ModbusHoldingRegister>> holdingLists = holdingRegisterClusters.stream().map(cluster -> ClusterHelper.getRegisters(cluster)).collect(Collectors.toList());

        // Re-map to readAsync requests
        coilRequests = coilLists.stream().map(list -> new ReadCoilRequests(list.stream().map(reg -> new ReadCoilRequest(reg)).toArray(ReadCoilRequest[]::new))).collect(Collectors.toList());
        discreteRequests = discreteLists.stream().map(list -> new ReadDiscreteRequests(list.stream().map(reg -> new ReadDiscreteRequest(reg)).toArray(ReadDiscreteRequest[]::new))).collect(Collectors.toList());
        inputRequests = inputLists.stream().map(list -> new ReadInputRequests(list.stream().map(reg -> new ReadInputRequest(reg)).toArray(ReadInputRequest[]::new))).collect(Collectors.toList());
        holdingRequests = holdingLists.stream().map(list -> new ReadHoldingRequests(list.stream().map(reg -> new ReadHoldingRequest(reg)).toArray(ReadHoldingRequest[]::new))).collect(Collectors.toList());
    }

    public ModbusRegister<?> getRegister(String ref) {
        return keyMap.get(ref);
    }

    public void update() {
        coilRequests.forEach(req -> {
            try {
                List<ReadCoilResponse<?>> coilResponses = wrapper.read(req);
                coilResponses.forEach(res -> event(res));
            } catch (Exception e) {
                // TODO Exception handling
            }
        });

        discreteRequests.forEach(req -> {
            try {
                List<ReadDiscreteResponse<?>> discreteResponses = wrapper.read(req);
                discreteResponses.forEach(res -> event(res));
            } catch (Exception e) {
                // TODO Exception handling
            }
        });

        inputRequests.forEach(req -> {
            try {
                List<ReadInputResponse<?>> inputResponses = wrapper.read(req);
                inputResponses.forEach(res -> event(res));
            } catch (Exception e) {
                // TODO Exception handling
            }
        });

        holdingRequests.forEach(req -> {
            try {
                List<ReadHoldingResponse<?>> holdingResponses = wrapper.read(req);
                holdingResponses.forEach(res -> event(res));
            } catch (Exception e) {
                // TODO Exception handling
            }
        });
    }

    public <T> ReadResponse<T> read(String key) {
        ModbusRegister<?> register = keyMap.get(key);

        if (register instanceof ModbusCoilRegister) {
            ReadCoilRequest req = new ReadCoilRequest((ModbusCoilRegister) register);
            ReadCoilResponse res = wrapper.read(req);
            return res;

        } else if (register instanceof ModbusDiscreteRegister) {
            ReadDiscreteRequest req = new ReadDiscreteRequest((ModbusDiscreteRegister) register);
            ReadDiscreteResponse res = wrapper.read(req);
            return res;

        } else if (register instanceof ModbusInputRegister) {
            ReadInputRequest req = new ReadInputRequest((ModbusInputRegister) register);
            ReadInputResponse res = wrapper.read(req);
            return res;

        } else if (register instanceof ModbusHoldingRegister) {
            ReadHoldingRequest req = new ReadHoldingRequest((ModbusHoldingRegister) register);
            ReadHoldingResponse res = wrapper.read(req);
            return res;

        } else {
            throw new IllegalArgumentException(String.format("Unable to read from register type %s", register.getClass().getSimpleName()));
        }
    }

    public <T> WriteResponse<T> write(String key, T value) {
        ModbusRegister<?> register = keyMap.get(key);

        if (register instanceof ModbusCoilRegister) {
            WriteCoilRequest req = new WriteCoilRequest((ModbusCoilRegister) register, value);
            WriteCoilResponse res = wrapper.write(req);
            return res;

        } else if (register instanceof ModbusHoldingRegister) {
            WriteHoldingRequest req = new WriteHoldingRequest((ModbusHoldingRegister) register, value);
            WriteHoldingResponse res = wrapper.write(req);
            return res;

        } else {
            throw new IllegalArgumentException(String.format("Unable to write to register type %s", register.getClass().getSimpleName()));
        }
    }

    private <T> void event(ReadResponse<T> response) {
        if (regMap.containsKey(response.getRegister())) {
            String key = regMap.get(response.getRegister());

            event(key, response.getRegister(), response.getValue());
        }
    }

    private <T> void event(String key, ModbusRegister<T> register, T value) {
        listeners.forEach(listener -> listener.update(key, register, value));
    }

    public void addEventListener(MapperUpdateListener listener) {
        listeners.add(listener);
    }
}
