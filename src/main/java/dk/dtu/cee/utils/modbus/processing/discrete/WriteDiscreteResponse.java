package dk.dtu.cee.utils.modbus.processing.discrete;

import dk.dtu.cee.utils.modbus.processing.WriteResponse;
import dk.dtu.cee.utils.modbus.registers.ModbusDiscreteRegister;

public class WriteDiscreteResponse<T> extends WriteResponse<T> {
    public WriteDiscreteResponse(ModbusDiscreteRegister<T> register) {
        super(register);
    }

    @Override
    public ModbusDiscreteRegister<T> getRegister() {
        return (ModbusDiscreteRegister)register;
    }
}
