package dk.dtu.cee.utils.modbus.processing.holding;

import dk.dtu.cee.utils.modbus.processing.ReadResponse;
import dk.dtu.cee.utils.modbus.registers.ModbusRegister;

public class ReadHoldingResponse<T> extends ReadResponse<T> {
    public ReadHoldingResponse(ModbusRegister<T> register, T value) {
        super(register, value);
    }
}
