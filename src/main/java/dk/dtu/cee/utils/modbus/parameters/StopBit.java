package dk.dtu.cee.utils.modbus.parameters;

public enum StopBit {

    One(1),
    Two(2);

    private final int value;

    StopBit(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
